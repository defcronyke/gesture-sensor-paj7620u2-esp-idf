# PAJ7620U2 i2c Gesture Sensor program for esp-idf

This is an esp-idf program to take readings from the i2c PAJ7620U2 Gesture Sensor with an esp32 microcontroller.

Configure it by running: idf.py menuconfig

Build, flash, and monitor its output with: idf.py flash monitor
