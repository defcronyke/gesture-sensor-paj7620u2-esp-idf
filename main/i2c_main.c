/* PAJ7620U2 Gesture Sensor i2c esp32 idf */
#include <stdio.h>
#include <string.h>
#include "esp_log.h"
#include "driver/i2c.h"
#include "sdkconfig.h"

static const char *TAG = "i2c-gesture-sensor";

#define _I2C_NUMBER(num) I2C_NUM_##num
#define I2C_NUMBER(num) _I2C_NUMBER(num)

#define DATA_LENGTH 512                  /*!< Data buffer length of test buffer */
#define RW_TEST_LENGTH 128               /*!< Data length for r/w test, [0,DATA_LENGTH] */
#define DELAY_TIME_BETWEEN_ITEMS_MS 1000 /*!< delay time between different test items */

#define I2C_MASTER_SCL_IO CONFIG_I2C_MASTER_SCL               /*!< gpio number for I2C master clock */
#define I2C_MASTER_SDA_IO CONFIG_I2C_MASTER_SDA               /*!< gpio number for I2C master data  */
#define I2C_MASTER_NUM I2C_NUMBER(CONFIG_I2C_MASTER_PORT_NUM) /*!< I2C port number for master dev */
#define I2C_MASTER_FREQ_HZ CONFIG_I2C_MASTER_FREQUENCY        /*!< I2C master clock frequency */
#define I2C_MASTER_TX_BUF_DISABLE 0                           /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0                           /*!< I2C master doesn't need buffer */

#define WRITE_BIT I2C_MASTER_WRITE              /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ                /*!< I2C master read */
#define ACK_CHECK_EN 0x1                        /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0                       /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                             /*!< I2C ack value */
#define NACK_VAL 0x1                            /*!< I2C nack value */

#define GESTURE_SENSOR_ADDR CONFIG_GESTURE_SENSOR_ADDR

#define SHIFT(n, shift) ( n << shift )

// Gesture detection interrupt flags.
#define PAJ_INT_FLAG1       0x43
#define PAJ_RIGHT		    SHIFT(1, 0)
#define PAJ_LEFT		    SHIFT(1, 1)
#define PAJ_UP			    SHIFT(1, 2)
#define PAJ_DOWN		    SHIFT(1, 3)
#define PAJ_FORWARD	    	SHIFT(1, 4)
#define PAJ_BACKWARD	    SHIFT(1, 5)
#define PAJ_CLOCKWISE       SHIFT(1, 6)
#define PAJ_COUNT_CLOCKWISE SHIFT(1, 7)
#define PAJ_WAVE			0x3

#define CHANGE_BANK_ADDR    0xEF
#define BANK0               0x00
#define BANK1               0x01

// Set the mode at Bank 1
#define MODE_ADDR   0x65
#define NORMAL_MODE 0xB7    // normal (far) mode 120 fps
#define GAMING_MODE 0x12    // gaming (near) mode 240 fps

#define GESTURE_DURATION CONFIG_GESTURE_DURATION

SemaphoreHandle_t print_mux = NULL;

// Register values for sensor initialization.
int init_register_array[][2] = {
    {0xEF, 0x00},
    {0x37, 0x07},
	{0x38, 0x17},
	{0x39, 0x06},
	{0x41, 0x00},
	{0x42, 0x00},
	{0x46, 0x2D},
	{0x47, 0x0F},
	{0x48, 0x3C},
	{0x49, 0x00},
	{0x4A, 0x1E},
	{0x4C, 0x20},
	{0x51, 0x10},
	{0x5E, 0x10},
	{0x60, 0x27},
	{0x80, 0x42},
	{0x81, 0x44},
	{0x82, 0x04},
	{0x8B, 0x01},
	{0x90, 0x06},
	{0x95, 0x0A},
	{0x96, 0x0C},
	{0x97, 0x05},
	{0x9A, 0x14},
	{0x9C, 0x3F},
	{0xA5, 0x19},
	{0xCC, 0x19},
	{0xCD, 0x0B},
	{0xCE, 0x13},
	{0xCF, 0x64},
	{0xD0, 0x21},
	{0xEF, 0x01},
	{0x02, 0x0F},
	{0x03, 0x10},
	{0x04, 0x02},
	{0x25, 0x01},
	{0x27, 0x39},
	{0x28, 0x7F},
	{0x29, 0x08},
	{0x3E, 0xFF},
	{0x5E, 0x3D},
	{0x65, 0x96},
	{0x67, 0x97},
	{0x69, 0xCD},
	{0x6A, 0x01},
	{0x6D, 0x2C},
	{0x6E, 0x01},
	{0x72, 0x01},
	{0x73, 0x35},
	{0x74, 0x00},
	{0x77, 0x01},
};

// Register values for gesture mode initialization.
int init_gesture_array[][2] = {
    {0xEF, 0x00},
	{0x41, 0x00},
	{0x42, 0x00},
	{0xEF, 0x00},
	{0x48, 0x3C},
	{0x49, 0x00},
	{0x51, 0x10},
	{0x83, 0x20},
	{0x9F, 0xF9},
	{0xEF, 0x01},
	{0x01, 0x1E},
	{0x02, 0x0F},
	{0x03, 0x10},
	{0x04, 0x02},
	{0x41, 0x40},
	{0x43, 0x30},
	{0x65, 0x96},
	{0x66, 0x00},
	{0x67, 0x97},
	{0x68, 0x01},
	{0x69, 0xCD},
	{0x6A, 0x01},
	{0x6B, 0xB0},
	{0x6C, 0x04},
	{0x6D, 0x2C},
	{0x6E, 0x01},
	{0x74, 0x00},
	{0xEF, 0x00},
	{0x41, 0xFF},
	{0x42, 0x01},
};

// Register values for proximity mode initialization.
int init_ps_array[][2] = {
    {0xEF, 0x00},
	{0x41, 0x00},
	{0x42, 0x00},
	{0x48, 0x3C},
	{0x49, 0x00},
	{0x51, 0x13},
	{0x83, 0x20},
	{0x84, 0x20},
	{0x85, 0x00},
	{0x86, 0x10},
	{0x87, 0x00},
	{0x88, 0x05},
	{0x89, 0x18},
	{0x8A, 0x10},
	{0x9f, 0xf8},
	{0x69, 0x96},
	{0x6A, 0x02},
	{0xEF, 0x01},
	{0x01, 0x1E},
	{0x02, 0x0F},
	{0x03, 0x10},
	{0x04, 0x02},
	{0x41, 0x50},
	{0x43, 0x34},
	{0x65, 0xCE},
	{0x66, 0x0B},
	{0x67, 0xCE},
	{0x68, 0x0B},
	{0x69, 0xE9},
	{0x6A, 0x05},
	{0x6B, 0x50},
	{0x6C, 0xC3},
	{0x6D, 0x50},
	{0x6E, 0xC3},
	{0x74, 0x05},
};

/**
 * @brief test code to read i2c slave device with registered interface
 * _______________________________________________________________________________________________________
 * | start | slave_addr + rd_bit +ack | register + ack | read n-1 bytes + ack | read 1 byte + nack | stop |
 * --------|--------------------------|----------------|----------------------|--------------------|------|
 *
 */
static esp_err_t i2c_master_read_slave_reg(i2c_port_t i2c_num, uint8_t i2c_addr, uint8_t i2c_reg, uint8_t* data_rd, size_t size)
{
    if (size == 0) {
        return ESP_OK;
    }
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    // first, send device address (indicating write) & register to be read
    i2c_master_write_byte(cmd, ( i2c_addr << 1 ), ACK_CHECK_EN);
    // send register we want
    i2c_master_write_byte(cmd, i2c_reg, ACK_CHECK_EN);
    // Send repeated start
    i2c_master_start(cmd);
    // now send device address (indicating read) & read data
    i2c_master_write_byte(cmd, ( i2c_addr << 1 ) | READ_BIT, ACK_CHECK_EN);
    if (size > 1) {
        i2c_master_read(cmd, data_rd, size - 1, ACK_VAL);
    }
    i2c_master_read_byte(cmd, data_rd + size - 1, NACK_VAL);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    return ret;
}

/**
 * @brief Test code to write i2c slave device with registered interface
 *        Master device write data to slave(both esp32),
 *        the data will be stored in slave buffer.
 *        We can read them out from slave buffer.
 * ____________________________________________________________________________________
 * | start | slave_addr + wr_bit + ack | register + ack | write n bytes + ack  | stop |
 * --------|---------------------------|----------------|----------------------|------|
 *
 */
static esp_err_t i2c_master_write_slave_reg(i2c_port_t i2c_num, uint8_t i2c_addr, uint8_t i2c_reg, uint8_t* data_wr, size_t size)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    // first, send device address (indicating write) & register to be written
    i2c_master_write_byte(cmd, ( i2c_addr << 1 ) | WRITE_BIT, ACK_CHECK_EN);
    // send register we want
    i2c_master_write_byte(cmd, i2c_reg, ACK_CHECK_EN);
    // write the data
    i2c_master_write(cmd, data_wr, size, ACK_CHECK_EN);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
    return ret;
}

// Returns a string describing the gesture, given the numerical reading value as input.
const char* gesture_str(uint16_t ges) {
    switch (ges)
    {
        case PAJ_UP:
            return "up";
            break;
        
        case PAJ_DOWN:
            return "down";
            break;
        
        case PAJ_LEFT:
            return "left";
            break;

        case PAJ_RIGHT:
            return "right";
            break;

        case PAJ_FORWARD:
            return "forward";
            break;

        case PAJ_BACKWARD:
            return "backward";
            break;

        case PAJ_CLOCKWISE:
            return "clockwise";
            break;

        case PAJ_COUNT_CLOCKWISE:
            return "counter-clockwise";
            break;

        case PAJ_WAVE:
            return "wave";
            break;

        default:
            ESP_LOGI(TAG, "no gesture: %#02x", ges);
            return "none";
    }
}

/**
 * @brief test code to operate on gesture sensor
 *
 * 1. set operation mode(e.g One time L-resolution mode)
 * _________________________________________________________________
 * | start | slave_addr + wr_bit + ack | write 1 byte + ack  | stop |
 * --------|---------------------------|---------------------|------|
 * 2. wait more than 24 ms
 * 3. read data
 * ______________________________________________________________________________________
 * | start | slave_addr + rd_bit + ack | read 1 byte + ack  | read 1 byte + nack | stop |
 * --------|---------------------------|--------------------|--------------------|------|
 */
static esp_err_t i2c_master_sensor_test(i2c_port_t i2c_num, uint8_t *data_h, uint8_t *data_l)
{
    esp_err_t ret;

    // Fix for i2c timeout error suggested from here:
    // https://github.com/espressif/esp-idf/issues/680#issuecomment-486212233
    int set_timeout_ret = i2c_set_timeout(i2c_num, 1048575);
    if (set_timeout_ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed setting i2c timeout: %s", esp_err_to_name(set_timeout_ret));
        return set_timeout_ret;
    }


    // Check if sensor is ready.
    uint8_t *ready = (uint8_t*)malloc(sizeof(uint8_t));
    while (ready == NULL || *ready != 0x20) {
        // Wait for sensor to stabilize.
        vTaskDelay(500 / portTICK_RATE_MS);

        ESP_LOGI(TAG, "Checking if sensor is ready...");

        ret = i2c_master_read_slave_reg(i2c_num, GESTURE_SENSOR_ADDR, 0x00, ready, 1);

        if (ready == NULL) {
            ESP_LOGE(TAG, "Failed checking if sensor is ready (ready = NULL, ret = %s). Checking again...", esp_err_to_name(ret));
            continue;
        }

        if (ret != ESP_OK || *ready != 0x20) {
            ESP_LOGW(TAG, "Sensor isn't ready yet (ready = %#02x, ret = %s). Checking again...", *ready, esp_err_to_name(ret));
        }
    }

    free((void*)ready);

    ESP_LOGI(TAG, "Sensor is ready.");
    
    vTaskDelay(30 / portTICK_RATE_MS);


    // Initialize sensor.

    // Get length of array.
    size_t reg_arr_len = sizeof init_register_array / sizeof *init_register_array;

    for (int i = 0; i < reg_arr_len; i++) {
        ESP_LOGI(TAG, "Initializing sensor state: {%#02x, %#02x}", init_register_array[i][0], init_register_array[i][1]);

        ret = i2c_master_write_slave_reg(i2c_num, GESTURE_SENSOR_ADDR, init_register_array[i][0], (uint8_t*)&init_register_array[i][1], 1);
        if (ret != ESP_OK) {
            return ret;
        }
    }

    ESP_LOGI(TAG, "Sensor is initialized.");

    vTaskDelay(30 / portTICK_RATE_MS);


    // Initialize gesture mode (as opposed to proximity mode).

    // Get length of array.
    size_t gest_arr_len = sizeof init_gesture_array / sizeof *init_gesture_array;

    for (int i = 0; i < gest_arr_len; i++) {
        ESP_LOGI(TAG, "Initializing gesture mode: {%#02x, %#02x}", init_gesture_array[i][0], init_gesture_array[i][1]);

        ret = i2c_master_write_slave_reg(i2c_num, GESTURE_SENSOR_ADDR, init_gesture_array[i][0], (uint8_t*)&init_gesture_array[i][1], 1);
        if (ret != ESP_OK) {
            return ret;
        }
    }

    ESP_LOGI(TAG, "Gesture mode is initialized.");

    vTaskDelay(30 / portTICK_RATE_MS);


    // Enable Normal Mode.
    // ESP_LOGI(TAG, "Enabling normal mode...");

    // Enable Gaming Mode.
    ESP_LOGI(TAG, "Enabling gaming mode...");
    
    // Set i2c write mode.
    uint8_t bank = BANK1;
    ret = i2c_master_write_slave_reg(i2c_num, GESTURE_SENSOR_ADDR, CHANGE_BANK_ADDR, &bank, 1);
    if (ret != ESP_OK) {
        return ret;
    }

    // uint8_t mode = NORMAL_MODE;
    uint8_t mode = GAMING_MODE;
    ret = i2c_master_write_slave_reg(i2c_num, GESTURE_SENSOR_ADDR, MODE_ADDR, &mode, 1);
    if (ret != ESP_OK) {
        return ret;
    }

    bank = BANK0;
    ret = i2c_master_write_slave_reg(i2c_num, GESTURE_SENSOR_ADDR, CHANGE_BANK_ADDR, &bank, 1);
    if (ret != ESP_OK) {
        return ret;
    }

    // ESP_LOGI(TAG, "Normal mode is enabled.");
    ESP_LOGI(TAG, "Gaming mode is enabled.");

    vTaskDelay(30 / portTICK_RATE_MS);

    return ret;
}

/**
 * @brief i2c master initialization
 */
static esp_err_t i2c_master_init()
{
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    i2c_param_config(i2c_master_port, &conf);
    return i2c_driver_install(i2c_master_port, conf.mode,
                              I2C_MASTER_RX_BUF_DISABLE,
                              I2C_MASTER_TX_BUF_DISABLE, 0);
}

static void i2c_test_task(void *arg)
{
    int ret;
    uint32_t task_idx = (uint32_t)arg;
    uint8_t sensor_data_h, sensor_data_l;
    int cnt = 0;
    while (1) {
        ESP_LOGI(TAG, "TASK[%d] test cnt: %d", task_idx, cnt++);
        ret = i2c_master_sensor_test(I2C_MASTER_NUM, &sensor_data_h, &sensor_data_l);
        xSemaphoreTake(print_mux, portMAX_DELAY);
        if (ret == ESP_ERR_TIMEOUT) {
            ESP_LOGE(TAG, "I2C Timeout");
        } else if (ret == ESP_OK) {
            // Initialization succeeded.
            for (;;) {
                uint8_t* ges = (uint8_t*)malloc(sizeof(uint8_t) * 2);

                ret = i2c_master_read_slave_reg(I2C_MASTER_NUM, GESTURE_SENSOR_ADDR, PAJ_INT_FLAG1, ges, sizeof(uint8_t) * 2);

                if (ret != ESP_OK) {
                    if (ges != NULL) {
                        free((void*)ges);
                    }

                    vTaskDelay((DELAY_TIME_BETWEEN_ITEMS_MS * (task_idx + 1)) / portTICK_RATE_MS);
                    continue;
                }

                if (ges != NULL) {
                    const char* ges_str = gesture_str(*ges);
                    
                    if (strcmp(ges_str, "none") != 0) {
                        ESP_LOGI(TAG, "Gesture detected: %s", ges_str);
                    }

                    free((void*)ges);
                }

                vTaskDelay(GESTURE_DURATION / portTICK_PERIOD_MS);
            }
        } else {
            ESP_LOGW(TAG, "%s: No ack, sensor not connected...skip...", esp_err_to_name(ret));
        }
        xSemaphoreGive(print_mux);
        vTaskDelay((DELAY_TIME_BETWEEN_ITEMS_MS * (task_idx + 1)) / portTICK_RATE_MS);
    }
    vSemaphoreDelete(print_mux);
    vTaskDelete(NULL);
}

void app_main()
{
    print_mux = xSemaphoreCreateMutex();
    ESP_ERROR_CHECK(i2c_master_init());
    xTaskCreate(i2c_test_task, "i2c_test_task_1", 1024 * 2, (void *)1, 10, NULL);
}
